"""
creating symmetric simulated galaxies with the structural parameters we measured in Buitrago+23

Things to improve:
- Revise all asumptions for the initial values
- Radii should make sense in kpc, by now everything is in pixel units
- Add to everything vectors to be written in the catalogs, i.e. pa_bulge, ellip_bulge, pa_halo... and not as it is now
"""

#Usage python creating_sim_truncs.py MODE NUM_SIMS STAMP_SIZE
#MODE: train/test


import glob
from astropy.io import fits
import numpy as np
import pandas as pd
#import montage_wrapper as montage
import os.path
import pdb
import scipy.constants as const
import os
import sys
import cv2


def boost_component(models_folder, sim_file, zeropoint, target_mag):
    hdu = fits.open(models_folder+sim_file)
    img = hdu[0].data
    hdr = hdu[0].header
    flux = np.sum(img)
    mag = -2.5*np.log10(flux)+zeropoint
    
    #boosting to the desired magnitude
    #based on mag1-mag2 = -2.5*log10(flux1/flux2)
    img_final = (10**((mag-target_mag)/2.5))*img 
    
    return img_final
    
def run_imfit(base_image,models_folder,config_file,sim_file,psf_image_filename):
    command = "makeimage --refimage="+base_image+" "+models_folder+config_file+" -o "+models_folder+sim_file+" -psf "+psf_image_filename
    print(command)
    os.system(command)
    
def main():
    ref_image_filename = "/mnt/disk3/hlsp_candels_hst_wfc3_cos-tot_f160w_v1.0_drz.fits" #galaxy field image
    psf_image_filename = "./real_psf_h_band.fits"
    path= "./models/"
    flag_saving_individual_components = False #do you want to save fits files of the disk/bulge/halo components?
    pix_scale = 0.06 #arcsec/pix
    min_trunc_size_pix = 10 #[pix], minimum truncation size to be simulated
    max_trunc_size_pix = 180 #[pix], maximum truncation size to be simulated
    min_bulge_size_pix = 1 #[pix], minimum truncation size to be simulated
    max_bulge_size_pix = 5 #[pix], maximum truncation size to be simulated
    min_bulge_nn = 2 #min Sersic index bulge
    max_bulge_nn = 5 #max Sersic index bulge
    
    mode = str(sys.argv[1])
    no_sims = int(sys.argv[2]) #number of galaxies to be simulated
    stamp_size = int(sys.argv[3]) #size of the output galaxy stamps
    

    #reading the catalogs with the galaxies we want to simulate
    gals  = [ii for ii in range(no_sims)] 
    field = np.full(no_sims, "cosmos") #[ii for ii in range(no_sims)] 
    #==Options for the disk component
    pa    = np.random.random(no_sims)*180
    ar    = np.random.random(no_sims); incl = np.arccos(ar)*(360/(2*const.pi))
    h1    = np.full(no_sims,10)
    h2    = np.full(no_sims,1)
    r_edge_pix = np.random.uniform(min_trunc_size_pix, max_trunc_size_pix, size = no_sims)
    alpha = np.full(no_sims,0.15)
    nn    = np.full(no_sims,1)
    z_0   = np.full(no_sims,2)
    I_0   = np.full(no_sims,1) #because I don't know how to predict the value of this parameter beforehand, I will later boost the flux to the desired final magnitude
    #our histogram of H-band magnitudes for our 1048 galaxies is a gaussian between 17 and 23 mag, that peaks at 20.5 and FHWM ~1mag
    if mode == 'train':
        #Uniform distribution cut at 2sigma
        print('Uniform distribution for magnitudes')
        target_mag = np.random.uniform(low = 18.5, high=22.5, size = no_sims)
    if mode == 'test':
        #following what we see in our HST images: our histogram of H-band magnitudes for our 1048 galaxies is a gaussian between 17 and 23 mag, that peaks at 20.5 and FHWM ~1mag
        print('Normal distribution for magnitudes')
        target_mag = np.random.normal(loc = 20.5, scale = 1, size = no_sims)
    #==Options for the bulge component
    re_bulge = np.random.uniform(min_bulge_size_pix, max_bulge_size_pix, size = no_sims)
    nn_bulge = np.random.uniform(min_bulge_nn, max_bulge_nn, size = no_sims)
    target_mag_bulge = (target_mag + 4.) - np.random.uniform(0, 2, size = no_sims)
    #==Options for the halo component
    target_mag_halo = (target_mag_bulge + 2) - np.random.uniform(0, 1, size = no_sims)


    #reading the galaxy field image
    hdu = fits.open(ref_image_filename)
    img_field = hdu[0].data
    hdr_field = hdu[0].header
    dim1 = hdr_field["NAXIS1"]
    dim2 = hdr_field["NAXIS2"]

    fits.writeto("tmp.fits",np.zeros((stamp_size,stamp_size)), overwrite=True)

    x0_vector = np.array([])
    y0_vector = np.array([])
    x1_vector = np.array([])
    y1_vector = np.array([])
    mag_disk_vector  = np.array([])
    mag_bulge_vector = np.array([])
    mag_halo_vector  = np.array([])
    J_0_vector = np.array([])
    mag_bulge_vector = np.array([])
    I_0_bulge_vector = np.array([])
    I_0_halo_vector = np.array([])
    for ii,gal in enumerate(gals):
        
        models_label = str(int(stamp_size)) + 'px'
        data_folder = path+"Imfit_" + models_label
        if mode == 'train':
            models_folder = data_folder + "/Imfit_Training_Data/"
        if mode == 'test':
            models_folder = data_folder + "/Imfit_Test_Data/"
        
        if not os.path.exists(models_folder):
            os.makedirs(models_folder) #instead of os.mkdir, os.makedirs it is able to create several nested folders at a time

        #cutting the random stamp where the simulated galaxy will be superimposed====================================================
        flag_cutted = False
        while flag_cutted == False:
            x_center = np.random.randint(0, dim1)
            y_center = np.random.randint(0, dim2)
            
            #I will take a cutout in the new centroid position with the dimensions of the simulated galaxy
            pix_in_x_halfsize = int(stamp_size/2)
            pix_in_y_halfsize = int(stamp_size/2)
            cutout = img_field[y_center-pix_in_y_halfsize:y_center+pix_in_y_halfsize, x_center-pix_in_x_halfsize:x_center+pix_in_x_halfsize]
            
            #I will accept this new image if less than 10% of the pixels are zeros and the dimensions are all right
            if np.count_nonzero(cutout == 0) >= cutout.size/10 or cutout.shape[0] != stamp_size or cutout.shape[1] != stamp_size:
                flag_cutted = False
            else:
                flag_cutted = True
                
        #montage.mSubimage_pix(ref_image_filename, "tmp.fits", x_center-(stamp_size/2), y_center-(stamp_size/2), xpixsize = stamp_size-1, ypixsize = stamp_size-1 )
        
        #storing the positions where we cut the galaxy survey image
        x0_vector = np.append(int(x_center-pix_in_x_halfsize), x0_vector)
        y0_vector = np.append(int(y_center-pix_in_y_halfsize), y0_vector)
        x1_vector = np.append(int(x_center+pix_in_x_halfsize), x1_vector)
        y1_vector = np.append(int(y_center+pix_in_y_halfsize), y1_vector)
        #============================================================================================================================    
        
        #creating the disk component=================================================================================================
        J_0 = I_0/(2*z_0) #from the face-on case in a pure exponential disk
        
        config_file        = str(gal)+"_"+str(field[ii])+"_conf_disk_imfit.txt"
        sim_file           = str(gal)+"_"+str(field[ii])+"_disk_convolved.fits"
        seg_file           = str(gal)+"_"+str(field[ii])+"_Segmentation.fits"
        seg_file_png       = str(gal)+"_"+str(field[ii])+"_Segmentation.png"
        with open(models_folder + config_file,"w") as file_obj:
            file_obj.write("X0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("Y0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("FUNCTION BrokenExponentialDisk3D\n")
            file_obj.write("PA   "+str(pa[ii])+"  # Position angle\n")
            file_obj.write("inc  "+str(incl[ii])+"   # cos^-1 (b/a)\n")
            file_obj.write("J_0  "+str(J_0[ii])+" # Central luminosity density (on area)\n")
            file_obj.write("h1   "+str(h1[ii])+" # Scale length of the disc (pix) (Intensity decrease by factor e)\n")
            file_obj.write("h2   "+str(h2[ii])+" # Scale length halo (pix)\n")
            file_obj.write("r_break "+str(r_edge_pix[ii])+'  # Break radius - "transition"\n')
            file_obj.write("alpha "+str(alpha[ii])+" # The smaller the smoother the break\n")
            file_obj.write("n    "+str(nn[ii])+"\n")
            file_obj.write("z_0  "+str(z_0[ii])+" # Vertical scale height\n")
            
        run_imfit("tmp.fits",models_folder,config_file,sim_file,psf_image_filename)
            
        disk_flux_img = boost_component(models_folder, sim_file, 25.9463, target_mag[ii])

        #obtaining new parameters after boosting
        disk_flux_tot = np.sum(disk_flux_img)
        mag_disk_final = -2.5*np.log10(disk_flux_tot)+25.9463
        J_0_final = np.max(disk_flux_img)
        
        #saving the values given to the different magnitudes
        mag_disk_vector = np.append(mag_disk_vector, mag_disk_final)
        J_0_vector = np.append(J_0_vector, J_0_final)
        if flag_saving_individual_components == True:
            fits.writeto(models_folder+sim_file         ,disk_flux_img         ,hdr_field,overwrite=True)
        else:
            os.system("rm "+models_folder+sim_file)
        #=============================================================================================================================
        
        #creating the bulge component=================================================================================================    
        config_file        = str(gal)+"_"+str(field[ii])+"_conf_bulge_imfit.txt"
        sim_file           = str(gal)+"_"+str(field[ii])+"_bulge_convolved.fits"
        with open(models_folder + config_file,"w") as file_obj:
            file_obj.write("X0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("Y0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("FUNCTION Sersic\n")
            file_obj.write("PA   "+str(pa[ii])+"  # Position angle\n")
            file_obj.write("ell  "+str(0.)+"   # ellipticity (1-axis_ratio)\n")
            file_obj.write("n  "+str(nn_bulge[ii])+" # Central luminosity density (on area)\n")
            file_obj.write("I_e  "+str(J_0[ii]/10)+" # Intensity at the effective radius\n")
            file_obj.write("r_e   "+str(re_bulge[ii])+" # Effective radius of the bulge (pix)\n")
        
        run_imfit("tmp.fits",models_folder,config_file,sim_file,psf_image_filename)

        bulge_flux_img = boost_component(models_folder, sim_file, 25.9463, target_mag_bulge[ii])

        #obtaining new parameters after boosting
        bulge_flux_tot = np.sum(bulge_flux_img)
        mag_bulge_final = -2.5*np.log10(bulge_flux_tot)+25.9463
        I_0_bulge_final = np.max(bulge_flux_img)
        
        #saving the values given to the different magnitudes
        mag_bulge_vector = np.append(mag_bulge_vector, mag_bulge_final)
        I_0_bulge_vector = np.append(I_0_bulge_vector, I_0_bulge_final)
        if flag_saving_individual_components == True:
            fits.writeto(models_folder+sim_file         ,bulge_flux_img         ,hdr_field,overwrite=True)
        else:
            os.system("rm "+models_folder+sim_file)
        #=============================================================================================================================
        
        #creating the halo component=================================================================================================    
        config_file        = str(gal)+"_"+str(field[ii])+"_conf_halo_imfit.txt"
        sim_file           = str(gal)+"_"+str(field[ii])+"_halo_convolved.fits"
        with open(models_folder + config_file,"w") as file_obj:
            file_obj.write("X0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("Y0   "+str(np.round(stamp_size/2))+"\n")
            file_obj.write("FUNCTION Sersic\n")
            file_obj.write("PA   "+str(pa[ii])+"  # Position angle\n")
            file_obj.write("ell  "+str(0.)+"   # ellipticity (1-axis_ratio)\n")
            file_obj.write("n  "+str(1.)+" # Central luminosity density (on area)\n")
            file_obj.write("I_e  "+str(J_0[ii]/20)+" # Intensity at the effective radius\n")
            file_obj.write("r_e   "+str(re_bulge[ii]*10)+" # Effective radius of the bulge (pix)\n")
        
        run_imfit("tmp.fits",models_folder,config_file,sim_file,psf_image_filename)

        halo_flux_img = boost_component(models_folder, sim_file, 25.9463, target_mag_halo[ii])

        #obtaining new parameters after boosting
        halo_flux_tot = np.sum(halo_flux_img)
        mag_halo_final = -2.5*np.log10(halo_flux_tot)+25.9463
        I_0_halo_final = np.max(halo_flux_img)
        
        #saving the values given to the different magnitudes
        mag_halo_vector = np.append(mag_halo_vector, mag_halo_final)
        I_0_halo_vector = np.append(I_0_halo_vector, I_0_halo_final)
        if flag_saving_individual_components == True:
            fits.writeto(models_folder+sim_file         ,halo_flux_img         ,hdr_field,overwrite=True)
        else:
            os.system("rm "+models_folder+sim_file)
        #=============================================================================================================================
        
        #writing the final galaxy as a combination of all 2D profiles (disk + bulge + halo)
        sim_file_all_together = str(gal)+"_"+str(field[ii])+".fits"
        img_final_all_together = cutout + disk_flux_img + bulge_flux_img + halo_flux_img
        fits.writeto(models_folder+sim_file_all_together,img_final_all_together,hdr_field,overwrite=True)
        
        #writing ancilliary files
        center = (int((stamp_size)/2),int((stamp_size)/2))
        axes = (int(np.round(r_edge_pix[ii]/2)),int(np.round(r_edge_pix[ii]*ar[ii]/2)))
        angle = pa[ii]+90
        #Seg map for png
        mask = np.zeros((stamp_size, stamp_size), np.uint8)
        mask = cv2.ellipse(mask,center,axes,angle,0,360,255,-1)
        cv2.imwrite(models_folder+seg_file_png, mask)
        #Seg map for fits
        mask = np.zeros((stamp_size, stamp_size), np.uint8)
        mask = cv2.ellipse(mask,center,axes,angle,0,360,1,-1)
        fits.writeto(models_folder+seg_file, mask, hdr_field, overwrite=True)
        
        
        
    df = pd.DataFrame({"gal":gals, "field":field, "J_0": J_0_vector, "mag": mag_disk_vector, "r_edge": r_edge_pix, 
                       "h1": h1, "h2": h2, "ar": ar, "incl": incl, "pa": pa, "alpha": alpha, "n": nn, "z_0": z_0,
                       "x0": x0_vector, "y0": y0_vector, "x1": x1_vector, "y1": y1_vector})
    df.to_csv("simulated_galaxies_with_truncations_disks_parameters.csv", index = False)

    df = pd.DataFrame({"gal":gals, "field":field, "I_0": I_0_bulge_vector, "mag": mag_bulge_vector, "r_e": re_bulge, "n": nn_bulge,
                       "x0": x0_vector, "y0": y0_vector, "x1": x1_vector, "y1": y1_vector})
    df.to_csv("simulated_galaxies_with_truncations_bulges_parameters.csv", index = False)

    df = pd.DataFrame({"gal":gals, "field":field, "J_0": J_0_vector, "mag": mag_halo_vector, "r_0": I_0_halo_vector, "r_e": re_bulge*10,
                       "x0": x0_vector, "y0": y0_vector, "x1": x1_vector, "y1": y1_vector})
    df.to_csv("simulated_galaxies_with_truncations_haloes_parameters.csv", index = False)

    os.system("rm tmp.fits")
    

if __name__ == "__main__":
    main()
